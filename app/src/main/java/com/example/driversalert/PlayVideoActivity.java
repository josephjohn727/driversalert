package com.example.driversalert;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

public class PlayVideoActivity extends AppCompatActivity {

    private VideoView mVideoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        mVideoView = findViewById(R.id.videoView);

        Intent intent = getIntent();
        String reference_name = intent.getExtras().getString("uri_reference");
        Toast toast = Toast.makeText(getApplicationContext(),reference_name,Toast.LENGTH_SHORT);
        toast.show();
        String videoURL = "https://firebasestorage.googleapis.com/v0/b/drivers-alert-51fbe.appspot.com/o/videos%2F" + reference_name + "?alt=media&token=8cd66417-5672-47a1-8e56-bf103e0b79e9";

        Uri uri = Uri.parse(videoURL);
        mVideoView.setVideoURI(uri);
        mVideoView.start();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mVideoView.isPlaying()) {
            mVideoView.resume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mVideoView.pause();
    }
}
